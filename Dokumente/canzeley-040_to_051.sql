-- SQL-Datei zum Updaten der Datenbank Canzeley
-- von Version 040 nach Version 050

USE Canzeley;

-- Versionsnummer erhoehen

UPDATE `Systemeigenschaften` SET `EigenschaftWert` = '51' WHERE `EigenschaftName` = 'Datenbankversion';

--
-- Table structure for table `PersonenKonten`
--

DROP TABLE IF EXISTS `PersonenKonten`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PersonenKonten` (
  `ID` int(11) NOT NULL auto_increment,
  `AdressenID` int(11) NOT NULL,
  `KontenID` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PersonenKonten`
--

LOCK TABLES `PersonenKonten` WRITE;
/*!40000 ALTER TABLE `PersonenKonten` DISABLE KEYS */;
/*!40000 ALTER TABLE `PersonenKonten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KontenIBAN`
--

DROP TABLE IF EXISTS `KontenIBAN`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `KontenIBAN` (
  `ID` int(11) NOT NULL auto_increment,
  `Kontoinhaber` varchar(50) collate latin1_german1_ci default NULL,
  `Bank` varchar(50) collate latin1_german1_ci default NULL,
  `IBAN` varchar(34) collate latin1_german1_ci NOT NULL,
  `BIC` varchar(11) collate latin1_german1_ci default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `KontenIBAN`--

LOCK TABLES `KontenIBAN` WRITE;
/*!40000 ALTER TABLE `KontenIBAN` DISABLE KEYS */;
/*!40000 ALTER TABLE `KontenIBAN` ENABLE KEYS */;
UNLOCK TABLES;

